'use strict'

function ok (values, message, reply) {
    return reply
        .code(200)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({
            code : 200,
            message : message,
            values : values,
        })
}

function err (reply, e) {
    if (e.name === "UniqueViolationError") {
        return reply
            .code(500)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send({
                code : 500,
                message : e,
            })
    } else {
        return reply
            .code(500)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send({
                code : e.statusCode,
                message : e,
            })
    }
}

function notFound (values, message, reply) {
    return reply
        .code(400)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({
            code : 400,
            values : values,
            message : message,
        });
}

module.exports = {
    ok, err, notFound
}