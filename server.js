require('dotenv').config();

const fastify = require('fastify') ({
    logger: true
});

fastify.register(require('fastify-formbody'))
const customJwtAuth = require('./config/auth/auth')
fastify.register(customJwtAuth)


const routes = require('./routes/routes')

for (var key in routes) {
    var modules = routes[key]
    fastify.register(modules, { prefix: '/sync' })
}

// fastify.get('/signup', (req, reply) => {
// // authenticate the user.. are valid the credentials?
//     const token = fastify.jwt.sign({ hello: 'world' })
//     reply.send({ token })
// })

const start = async () => {
    try {
        await fastify.listen(process.env.PORT || 3000)
        console.log(`Server listening on ${fastify.server.address().port}`)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }
}

start()
