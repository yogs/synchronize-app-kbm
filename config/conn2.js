require('dotenv').config()

var cloud = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST2,
        user: process.env.DB_USERNAME2,
        password: process.env.DB_PASSWORD2,
        database: process.env.DB_DATABASE2,
        multipleStatements: true
    },
    pool: { min: 0, max: 3 } //Menggunakan fungsi pool agar menjaga koneksi ke DB tetep tersambung
})

module.exports = cloud