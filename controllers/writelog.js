'use strict';

const response_api = require('../response/res')
const knex = require('../config/conn')
const knex2 = require('../config/conn2')

const fs = require('fs')
const ftp = require("basic-ftp")

async function writeLog (request, reply) {
    var date_now = new Date().toLocaleString({
        timeZone: 'Asia/Jakarta'
      })
    date_now = new Date().getFullYear({timeZone: 'Asia/Jakarta'}) + "-" +
            ('00' + (new Date().getMonth({timeZone: 'Asia/Jakarta'})+1)).slice(-2) + '-' +
            ('00' + new Date().getDate({timeZone: 'Asia/Jakarta'})).slice(-2)

    const client = new ftp.Client()
    client.ftp.verbose = true
    try {
        await client.access({
            host: "localhost",
            user: "eois",
            password: "A#nd001.",
            secure: false
        })
        var startAt
        await client.downloadTo('queue_logs/user-log-'+ date_now +'.json', '/localcorenitymps-kbm.com/public/data/logs/user-log-'+ date_now +'.json', startAt = 0)
        await client.remove('/localcorenitymps-kbm.com/public/data/logs/user-log-'+ date_now +'.json')
        .then(function () {
            fs.readFile('queue_logs/user-log-'+ date_now +'.json', 'utf8', function(err, data) {
                if (err) throw err
                data = "[" + data + "]"
                let data_log = JSON.parse(data)
                fs.unlinkSync('queue_logs/user-log-'+ date_now +'.json')
                if (typeof data_log !== "undefined") {
                    knex.insert(data_log).into('tblog').then(function () {
                        return response_api.ok("", "Create Log User Success", reply)
                    }).catch ((error) => {
                        console.log(error)
                        return response_api.err(reply, error)
                    })
                }
            })
            // let rawdata = fs.readFileSync('queue_logs/user-log-'+ date_now +'.json')
        })
    }
    catch(err) {
        console.log(err)
        return response_api.err(reply, err)
    }
    // client.close()
}

async function writeLogCloud (request, reply) {
    var date_now = new Date().toLocaleString({
        timeZone: 'Asia/Jakarta'
      })
    date_now = new Date().getFullYear({timeZone: 'Asia/Jakarta'}) + "-" +
            ('00' + (new Date().getMonth({timeZone: 'Asia/Jakarta'})+1)).slice(-2) + '-' +
            ('00' + new Date().getDate({timeZone: 'Asia/Jakarta'})).slice(-2)

    const client = new ftp.Client()
    client.ftp.verbose = true
    try {
        await client.access({
            host: "157.230.37.200",
            user: "ftpmps-user",
            password: "A#nd007.",
            secure: false
        })
        var startAt
        await client.downloadTo('queue_logs/user-log-'+ date_now +'.json', '/corenitymps-kbm.com/public/data/logs/user-log-'+ date_now +'.json', startAt = 0)
        await client.remove('/corenitymps-kbm.com/public/data/logs/user-log-'+ date_now +'.json')
        .then(function () {
            fs.readFile('queue_logs/user-log-'+ date_now +'.json', 'utf8', function(err, data) {
                if (err) throw err
                data = "[" + data + "]"
                let data_log = JSON.parse(data)
                fs.unlinkSync('queue_logs/user-log-'+ date_now +'.json')
                if (typeof data_log !== "undefined") {
                    knex2.insert(data_log).into('tblog').then(function () {
                        return response_api.ok("", "Create Log User Success", reply)
                    }).catch ((error) => {
                        console.log(error)
                        return response_api.err(reply, error)
                    })
                }
            })
            // let rawdata = fs.readFileSync('queue_logs/user-log-'+ date_now +'.json')
        })
    }
    catch(err) {
        console.log(err)
        return response_api.err(reply, err)
    }
    // client.close()
}

module.exports = {
    writeLog, writeLogCloud
}