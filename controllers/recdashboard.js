'use strict';

const response_api = require('../response/res')
const knex = require('../config/conn')
const knex2 = require('../config/conn2')

async function record_dashbaord(request, reply) {
    knex.select().table('vwmst_karyawan_avail_tot').then(function(result) {
        knex.raw('call sp_dashboard_mou_tot()').then(function(result_mou) {
            knex.raw('call sp_asrdashboard_kecelakaan_gettotal()').then(function(result_asr_kec) {
                knex.raw('call sp_asrdasboard_gettotal()').then(function(result_asr) {
                    let sum_asr_kes = parseFloat(result_asr[0][0][0].asrkes_1) +  parseFloat(result_asr[0][0][0].asrkes_2) +  parseFloat(result_asr[0][0][0].asrkes_3) +  parseFloat(result_asr[0][0][0].asrkes_4) +  parseFloat(result_asr[0][0][0].asrkes_5) +  parseFloat(result_asr[0][0][0].asrkes_6)
                    let sum_asr_tk = parseFloat(result_asr[0][0][0].asrtk_1) +  parseFloat(result_asr[0][0][0].asrtk_2) +  parseFloat(result_asr[0][0][0].asrtk_3) +  parseFloat(result_asr[0][0][0].asrtk_4) +  parseFloat(result_asr[0][0][0].asrtk_5) +  parseFloat(result_asr[0][0][0].asrtk_6)
                    knex.table('tbtrx_spk').count('trx_kode as tot_spk').then(function(result_spk) {
                        const data_rec = {
                            group_klien : result[0].kli_grp_tot,
                            mou : result_mou[0][0][0].mou_active,
                            mou_20d : result_mou[0][0][0].mou_expired20d,
                            mou_exp : result_mou[0][0][0].mou_expired,
                            spk : result_spk[0].tot_spk,
                            pelamar : result[0].recruit_tot,
                            kontrak_kerja : result[0].assign_active_tot,
                            kontrak_kerja_20d : result[0].assign_expired20_tot,
                            kontrak_kerja_exp : result[0].assign_expired_tot,
                            kontrak_kera_satpam : result[0].kary_assign_tot_security,
                            kontrak_kerja_cs : result[0].kary_assign_tot_cleaning,
                            pkt : result[0].kary_pkt_tot,
                            asr_kes : sum_asr_kes,
                            asr_tk : sum_asr_tk,
                            asr_kecelakaan : result_asr_kec[0][0][0].sum_kecelakaan
                        }

                        knex('tbrec_dashboard_dialy').insert(data_rec).then(function(result) {
                            return response_api.ok("", "Sukses", reply)
                        }).catch ((error) => {
                            console.log(error)
                            return response_api.err(reply, error)
                        })
                    }).catch ((error) => {
                        console.log(error)
                        return response_api.err(reply, error)
                    })
                }).catch ((error) => {
                    console.log(error)
                    return response_api.err(reply, error)
                })
            }).catch ((error) => {
                console.log(error)
                return response_api.err(reply, error)
            })
        }).catch ((error) => {
            console.log(error)
            return response_api.err(reply, error)
        })
    }).catch ((error) => {
        console.log(error)
        return response_api.err(reply, error)
    })
}

module.exports = {
    record_dashbaord
}