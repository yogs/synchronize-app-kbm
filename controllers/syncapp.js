'use strict';

const response_api = require('../response/res')
const knex = require('../config/conn')
const knex_cloud = require('../config/conn2');
// var logger = require("../config/logger").Logger;

function syncOld (request, reply) {
    try {
        knex.select().table('tbsync')
        .then(function(result) {
            console.log(result)
            if (result.length > 0) {
                // Looping All row tbsync
                for (let i = 0; i < result.length; i++) {
                    console.log(result[i].tsql)
                    // knex_cloud.transaction(trx => {
                        // Exec query tsql
                        knex_cloud.raw(result[i].tsql)
                        // if success delete row in tbsync
                        .then(function(result_raw) {
                            console.log(result_raw)
                            console.log(result[i].trxid)
                            knex.table('tbsync').del().where({trxid: result[i].trxid})
                            .then(function(res_del) {
                                // trx.commit();
                                return response_api.ok("", "Sukses", reply)
                            })
                        })
                        // if error exec
                        .catch((error) => {
                            // trx.rollback()
                            return response_api.err(reply, error)
                        })
                    // })
                }
            }
        })
    } catch (e) {
        return response_api.err(reply, e)
    }
}

async function syncApp (request, reply) {
    knex.select().table('tbsync').then(async function(result) {
        if (result.length > 0) {
            for (let i=0; i<result.length; i++) {
                logger.info("Sukses Synchronize", result[i].tsql)
                let sync_cloud = await syncCloud(result[i].tsql)
                if (sync_cloud) {
                    let del_queue = await deleteQueue(result[i].trxid)
                    if (!del_queue) {
                        break;
                    }
                }
            }
            return response_api.ok("", "Sukses", reply)
        }
    }).catch((error) => {
        logger.error(error)
        console.log(error)
        return response_api.err(reply, error)
    })
}

async function syncCloud(tsql) {
    knex_cloud.raw(tsql).then(async function () {
        return true
    }).catch((error) => {
        logger.error(error)
        console.log(error)
        return false
    })
}

const deleteQueueOld = async function(trxid) {
    knex.table('tbsync').del().where({trxid: trxid}).then(async function () {
        return true
    }).catch((error) => {
        logger.error(error)
        console.log(error)
        return false
    })
}

function sync (request, reply) {
    knex.select().table('tbsync').where({cflag: '0'}).limit(25)
    .then(function(result) {
        if (result.length > 0) {
            for (let i = 0; i < result.length; i++) {

                knex('tbsync').where({trxid: result[i].trxid}).update({cflag: 1})
                .then(function () {

                    knex_cloud.raw(result[i].tsql)
                    .then(function() {
                        knex('tbsync').where({trxid: result[i].trxid}).update({cflag: 2})
                        .then(function () {
                        })
                    }).catch ((error) => {
                        knex('tbsync').where({trxid: result[i].trxid}).update({cflag: 0})
                        .then(function () {})
                        console.log(error)
                        // return response_api.err(reply, error)
                    })
                }).catch ((error) => {
                    console.log(error)
                    // return response_api.err(reply, error)
                })
            }
            return response_api.ok("", "Sukses", reply)
        }
    }).catch ((error) => {
        console.log(error)
        return response_api.err(reply, error)
    })
}

function deleteQueue(request, reply) {
    knex('tbsync').where({cflag:2}).delete()
    .then(function (result) {
        return response_api.ok("", "Hapus Queue Sukses", reply)
    }).catch ((error) => {
        console.log(error)
        return response_api.err(reply, error)
    })
}

module.exports = {
    sync, deleteQueue
}