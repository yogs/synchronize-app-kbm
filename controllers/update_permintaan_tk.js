'use strict';

const response_api = require('../response/res')
const knex = require('../config/conn')

async function update_permintaan_tk(request, reply) {
    knex.raw('call sp_permintaan_tk_auto_update()').then(function(result) {
        return response_api.ok("", "Sukses", reply)
    }).catch ((error) => {
        console.log(error)
        return response_api.err(reply, error)
    })
}

module.exports = {
    update_permintaan_tk
}