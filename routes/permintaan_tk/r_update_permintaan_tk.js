let permintaan_tk = require('../../controllers/update_permintaan_tk')

async function r_permintaan_tk (fastify, options) {
    // fastify.addHook('onRequest', fastify.authenticate)
    fastify.get('/update-permintaan-tk', permintaan_tk.update_permintaan_tk)
}

module.exports = r_permintaan_tk
