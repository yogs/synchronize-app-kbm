let log = require('../../controllers/writelog')

async function r_log (fastify, options) {
    // fastify.addHook('onRequest', fastify.authenticate)
    fastify.get('/writelog', log.writeLog)
    fastify.get('/writelog-cloud', log.writeLogCloud)
}

module.exports = r_log