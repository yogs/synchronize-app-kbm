// let login = require('./r_login.js')
let r_sync = require('./sync/r_sync')
let r_log = require('./logger_ui/r_log')
let r_rec_dash = require('./record_dashboard/r_rec_dashboard')
let r_permintaan_tk = require('./permintaan_tk/r_update_permintaan_tk')

module.exports = {  
    r_sync, r_log, r_rec_dash, r_permintaan_tk
}