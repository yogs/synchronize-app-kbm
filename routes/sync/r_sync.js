let sync = require('../../controllers/syncapp')

async function r_sync (fastify, options) {
    // fastify.addHook('onRequest', fastify.authenticate)
    fastify.get('/syncapp', sync.sync)
    fastify.get('/del-queue', sync.deleteQueue)
}

module.exports = r_sync
